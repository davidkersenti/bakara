<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>

<div class="wrap">

    <?php
    Yii::$app->user->isGuest ? $brandUrl = ['/site/index'] :
		$brandUrl = Yii::$app->homeUrl;
	           NavBar::begin([
                   
                'brandLabel' => Html::img('/project/image/kongo.png', ['alt'=>'Project Managment']),
                
                'brandUrl' => Yii::$app->homeUrl,
                'options' => [
                    'class' => '',
        ],
    ]);

    
 echo Nav::widget([
        'options' => ['class' => 'navbar-nav navbar-right'],
        'items' => [
				
			Yii::$app->user->isGuest ?
			['label' => 'Home', 'url' => ['/site/index']]
			:
			['label' => 'ERD', 'url' => ['/site/contact']]
			,
            Yii::$app->user->isGuest ? 
            ['label' => 'About', 'url' => ['/site/about']]
			:
            
            Yii::$app->user->isGuest ?
			['label' => 'Contact', 'url' => ['/site/contact']]
			:
             Yii::$app->user->isGuest ? 
            ['label' => 'About', 'url' => ['/site/about']]
			:
			'',
            
            
            
			
            // Yii::$app->user->isGuest ?
			// // ['label' => 'יצירת קשר', 'url' => ['/site/contact']]
			// // :
            // '': // if we want back contact delte  this line
			// ['label' => 'About', 'url' => ['/site/about']]
			// ,
           
            Yii::$app->user->isGuest ? 
            ''
			:
            ['label' => 'Roles',
				'items' => [
                    ['label' => 'Admin', 'url' => ['/role/index']],
                    ['label' => 'Ceo', 'url' => ['/role/create']],
                    ['label' => 'Project Manager', 'url' => ['/status/index']],
                    ['label' => 'Task Perform', 'url' => ['/status/create']],
            

           
					
                ],
            ],
            Yii::$app->user->isGuest ? 
            ''
			:
             ['label' => 'Main',
				'items' => [
                    ['label' => 'Projects', 'url' => ['/project/index']],
                    ['label' => 'Tasks', 'url' => ['/task/index']],
                    ['label' => 'Users', 'url' => ['/user/index']],

         
					
                ],
            ],
            
 Yii::$app->user->isGuest ?
			// ['label' => 'יצירת קשר', 'url' => ['/site/contact']]
			// :
            '': // if we want back contact delte  this line
			['label' => 'The System', 'url' => ['/userproject/index']]
			,
              Yii::$app->user->isGuest ?
			// ['label' => 'יצירת קשר', 'url' => ['/site/contact']]
			// :
            '': // if we want back contact delte  this line
			['label' => 'Home', 'url' => ['/site/index']]
			,

			
            Yii::$app->user->isGuest ? (
                ['label' => 'Login', 'url' => ['/site/login']]
            ) : (
                '<li>'
                . Html::beginForm(['/site/logout'], 'post')
                . Html::submitButton(
                    'Logout (' . Yii::$app->user->identity->username . ')',
                    ['class' => 'btn btn-link']
                )
                . Html::endForm()
                . '</li>'
            )
        ],
    ]);
    ?>
    <body bgcolor="#349" text="white" onload="startTime()">
            <h5 align="left">
            <span id="txt"></span>
            </h5>
        </body>
    <?php
    NavBar::end();
    ?>

    <div class="container">
         <?= Breadcrumbs::widget([
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [], 
        ]) ?>
        <script type="text/javascript">
        function startTime(){
            var d=new Date();
            //////// convert day into string
            var weekday = new Array(7);
            weekday[0] = "Sunday";
            weekday[1] = "Monday";
            weekday[2] = "Thesday";
            weekday[3] = "Wednesday";
            weekday[4] = "Thursday";
            weekday[5] = "Friday";
            weekday[6] = "Saturday";
            var n = weekday[d.getDay()];
            ////////// setting the date 
            var h=addZero(d.getHours());
            var m=addZero(d.getMinutes());
            var s=addZero(d.getSeconds());
            var day=d.getDate();
            var month=d.getMonth() +1;
            var year=d.getFullYear();
            document.getElementById("txt").innerHTML="<p>"+n+"</br>"+h+":"+m+":"+s+ "</br> " +day+ "/" +month+ "/" +year+"</p>";
            setTimeout('startTime()', 1000);
        }
        /// funtion for adding zero into the date
        function addZero(i) {
            if (i < 10) {
                i = "0" + i;
            }
            return i;
        }
        </script>
       
        <?= $content ?>
    </div>
</div>

<footer class="footer">
    <div class="container">
        <p class="pull-left">&copy; David Kersenti & Oz Yashar <?= date('Y') ?></p>

      
    </div>
</footer>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>